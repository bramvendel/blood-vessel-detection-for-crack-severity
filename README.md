# Blood Vessel Detection for Crack Severity

This project contains experimental and usable python code for the creation of a model for the automatic prediction of severity of cracks.
 
Script | Description
-------| -----------
run_final_model.py | Conatains the script to run the model and get predictions for every crack and saves it as excel file.
Full_tryout.ipynb | Contains the Python Notebook file on the trial en error prior to writing the complete script.
SA_UNet.py | Contains the structure for the model.
Model/CHASE/SA_UNet.h5| Contains the pre-trained weights for the model.
via_json_2_csv.py | Script to get a csv file from the via file. Output is input for run_final_model.py
main.py | Transforms the front view images to top view images and georeferences them. Result is a large tiff file with all of the roads segments tied together.
cut_defect_to_center.py | Code for changing the size of every image of each crack
run_test.py | Check to see if the model loads correctly.
preprocess_functions.py | Preprocessing functions necessary for run_final_model.py
get_individual_defects.py | Code to crop individual defects out of image.
Dropblock.py, flip.py, Spatial_attention.py, util.py, keras_dataAug.py| Necessary code to run the model. (Copied from Github Repository)

Python Notebooks | Description
-------| -----------
Full_tryout.ipynb & Notebook Results Bloodvessel.ipynb| Contains python notebook on exploratory data analysis and results. 

Folders | Description
-------| -----------
Predictions/CSV| Location where the CSV files and the predictions are saved once run_final_model.py is ran.