import geopandas
from shapely.geometry import Point
import shutil
from osgeo import gdal, osr
import tqdm
import os
import cv2
import pandas as pd
import numpy as np
import glob
import subprocess


# -----------------------------------------------------------------------------------------------------------------------------------------------------------------
# Set the Input/Output Paths
#------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Set the images path to the "Front View (Direction 0)"
IMAGES_PATH= glob.glob(r"N:\AAD\Data\Road\Images\Cyclomedia\Raw\Zeeland\N665_2020\All directions\N665_2020_direction_180\*.jpg")

# Set the JSON path to the defects
# FILE_PATH = r"N:\AAD\Data\Road\Images\Cyclomedia\Raw\Zeeland\Predictions_Zeeland_N665_2020\Latest_dutch_model_130000\N665_2020_direction_0_0.3_deleted_small_labels_1000.json"

# Set the path to the shapefile for the positioned polygons of the roads from cyclo.

shp_file_path = r'C:\Users\vendelb5569\OneDrive - ARCADIS\Top View N665\XMLs\N665_deg180_3_total.shp'

# -----------------------------------------------------------------------------------------------------------------------------------------------------------------
# Run the code to get merged big tiff file
#------------------------------------------------------------------------------------------------------------------------------------------------------------------

# def get_length_width(polygon, epsg):
#     if polygon.type == "MultiPolygon":
#         polygon = list(polygon)[0]
#     ##check again
#     coordinates = sorted(list(polygon.exterior.coords), key=lambda x: x[0])
#     min_x = Point(coordinates[0], epsg)
#     max_x = Point(coordinates[-1], epsg)

#     coordinates = sorted(list(polygon.exterior.coords), key=lambda x: x[1])
#     min_y = Point(coordinates[0], epsg)
#     max_y = Point(coordinates[-1], epsg)

#     length = min_x.distance(max_x)
#     width = min_y.distance(max_y)

#     if length < width:
#         return width, length
#     return length, width


# shp_file = geopandas.read_file(shp_file_path)
# list_r = [get_length_width(geom, epsg=shp_file.crs) for geom in shp_file['geometry']]
# ratio = np.mean([x / y for x, y in list_r])

# img_name_coords = {}

# for index in range(len(shp_file)):
#     x, y = shp_file.at[index, 'geometry'].exterior.coords.xy
#     x = x.tolist()[:-1]
#     y = y.tolist()[:-1]
#     image_id = shp_file.at[index, 'Image ID']
#     img_name_coords[image_id] = (x, y)

# width = 1000
# height = int(width * ratio)

# img_height = 2304
# img_width = 4096


# pts = np.array([[992, 2284], [1723, 1372], [2560, 1324], [3579, 2294]], dtype=np.float32)

# dts = np.array([[0, height], [0, 0], [width, 0], [width, height]], dtype=np.float32)
# matrix = cv2.getPerspectiveTransform(pts, dts)

# for img_path in tqdm.tqdm(IMAGES_PATH):

#     img_id = os.path.basename(img_path)[4:-4]
#     img = cv2.imread(img_path)
#     output = cv2.warpPerspective(img, matrix, (width, height))
#     output_path_img = os.path.join(r"C:\Users\vendelb5569\OneDrive - ARCADIS\Top View N665\output3\images", img_id + ".tif")
#     cv2.imwrite(output_path_img, output)
#     output_path_tif = os.path.join(r"C:\Users\vendelb5569\OneDrive - ARCADIS\Top View N665\output3\tiffs", img_id + ".tif")

#     x_coords, y_coords = img_name_coords[img_id]

#     coords = list(zip(x_coords, y_coords))

#     top_left = coords[1]
#     top_right = coords[2]
#     bot_left = coords[0]
#     bot_right = coords[3]

#     # Create a copy of the original file and save it as the output filename:
#     shutil.copy(output_path_img, output_path_tif)
#     sr = osr.SpatialReference()
#     res = sr.ImportFromEPSG(28992)

#     # Open the output file for writing:
#     ds = gdal.Open(output_path_tif, gdal.GA_Update)
#     ds.SetProjection(sr.ExportToWkt())

#     gcps = [gdal.GCP(top_left[0], top_left[1], 0, 0, 0),
#             gdal.GCP(top_right[0], top_right[1], 0, width, 0),
#             gdal.GCP(bot_left[0], bot_left[1], 0, 0, height),
#             gdal.GCP(bot_right[0], bot_right[1], 0, width, height)]

#     # Apply the GCPs to the open output file:
#     ds.SetGCPs(gcps, sr.ExportToWkt())

#     b1 = ds.GetRasterBand(1)
#     b1.SetNoDataValue(0)

#     gdal.Warp(os.path.join(r"C:\Users\vendelb5569\OneDrive - ARCADIS\Top View N665\output3\tiffs_geo", img_id + ".tif"), ds, dstSRS="EPSG:28992", format='GTiff')

#     ds = None

vrt = gdal.BuildVRT(r"C:\Users\vendelb5569\OneDrive - ARCADIS\Top View N665\output3\merged.vrt", glob.glob(r"C:\Users\vendelb5569\OneDrive - ARCADIS\Top View N665\output3\tiffs_geo\*.tif"))
vrt = None

cmd = ["gdal_translate", "-co", "compress=LZW" , r"C:\Users\vendelb5569\OneDrive - ARCADIS\Top View N665\output3\merged.vrt", r"C:\Users\vendelb5569\OneDrive - ARCADIS\Top View N665\output3\merged.tif"]
subprocess.Popen(cmd, shell=True)

print("Done")