import json
import os
import pandas as pd
import numpy as np
import xgboost
from shapely.geometry import Polygon
from centerline.geometry import Centerline
import cv2

from preprocess_functions import *

# ------------------------------------------------------------------------------------------------------------------------------------------
# Set the required paths: BASEPATH, file_name_excel, OUTPUT_PATH
# ------------------------------------------------------------------------------------------------------------------------------------------

BASEPATH = r'C:\Git-Clones\severity-model-road-defects\Nordics\Data\Overview\Overview_jsons.xlsx'

camera = 'Cyclomedia'           # Choose from Cyclomedia or Horus
angle = -7                      # Chroose from 0 or -7
country = 'Nordics'             # Choose NL, US, Nordics
methodology = 'CROW'            # Choose CROW, ASTM or RWS
old_new = 'new'                 # choose version of template 'old' or 'new'
top_view = 'Yes'                # Choose Yes or No (If cyclomedia is selected)

DEFECT_list = ['002. Crack longitudinal', '003. Crack transversal', '004. Crackles'] # Specify the defects that you want

OUTPUT_PATH = r"C:\Git-Clones\blood-vessel-detection-for-crack-severity\CSV" + "\\" + country + "\\" + camera + "_" + methodology + "_" + str(angle) +'.csv'

# ------------------------------------------------------------------------------------------------------------------------------------------
# Create the dataframe
# ------------------------------------------------------------------------------------------------------------------------------------------

total_jsons_df = pd.read_excel(BASEPATH)

# Create a list of the correct Jsons
total_jsons_df = total_jsons_df.loc[(total_jsons_df['Camera'] == camera) & 
                                    (total_jsons_df['Methodology'] == methodology) & 
                                    (total_jsons_df['Angle'] == angle) & 
                                    (total_jsons_df['Country'] == country)]

total_jsons_df_list = total_jsons_df['Json'].tolist()

total_df =  pd.DataFrame()
file_list = []
images_list = []
severity_list = []
defect_list = []
x_list = []
y_list = []
file_path_list = []
image_path_list = []
points_list = []



for file in total_jsons_df_list:
    filepath = file
    print("Currently doing file: ", file)
    
    with open(filepath) as json_file:
        data = json.load(json_file)
        file_path = data['_via_settings']['core']['default_filepath']

        for image in data['_via_img_metadata'].keys():

            image2 = data['_via_img_metadata'][image]['filename']

            for idx, i in enumerate(data['_via_img_metadata'][image]['regions']):
                severity = None
                defect = None
                xs = None
                ys = None

                

                if 'Severity' in data['_via_img_metadata'][image]['regions'][idx]['region_attributes']:

                    # OLD 
                    if old_new == 'old':
                        if '3' in data['_via_img_metadata'][image]['regions'][idx]['region_attributes']['Severity']:
                            severity = 3
                        if '2' in data['_via_img_metadata'][image]['regions'][idx]['region_attributes']['Severity']:
                            severity = 2
                        if '1' in data['_via_img_metadata'][image]['regions'][idx]['region_attributes']['Severity']:
                            severity = 1

                        # RWS
                        if '4' in data['_via_img_metadata'][image]['regions'][idx]['region_attributes']['Severity']:
                            severity = 3


                    # NEW
                    if old_new == 'new':
                        sev = data['_via_img_metadata'][image]['regions'][idx]['region_attributes']['Severity']
                    
                        if sev == 'L':
                            severity = 1
                    
                        if sev == 'M':
                            severity = 2
                            
                        if sev == 'E':
                            severity = 3
                
                
                if 'Defect' in data['_via_img_metadata'][image]['regions'][idx]['region_attributes']:
                    defect = data['_via_img_metadata'][image]['regions'][idx]['region_attributes']['Defect']

                if 'all_points_x' in data['_via_img_metadata'][image]['regions'][idx]['shape_attributes']:
                    xs =  data['_via_img_metadata'][image]['regions'][idx]['shape_attributes']['all_points_x']

                if 'all_points_y' in data['_via_img_metadata'][image]['regions'][idx]['shape_attributes']:    
                    ys = data['_via_img_metadata'][image]['regions'][idx]['shape_attributes']['all_points_y']

                # Only get the polygons that do have a shape, filter out None values
                if xs == None:
                    continue

                if ys == None:
                    continue

                # Add everything to the list
                file_list.append(file)
                severity_list.append(severity)
                defect_list.append(defect)
                x_list.append(xs)
                y_list.append(ys)
                images_list.append(image2)
                file_path_list.append(file_path)
                image_path = file_path + image2
                image_path_list.append(image_path)
                points_list.append(list(zip(xs, ys)))


total_df['File'] = file_list
total_df['Image'] = images_list
total_df['Severity'] = severity_list
total_df['Defect'] = defect_list
total_df['X_points'] = x_list
total_df['Y_points'] = y_list
total_df['File_path'] = file_path_list
total_df['Image_path'] = image_path_list
total_df['Points'] = points_list


# ------------------------------------------------------------------------------------------------------------------------------------------
# Get the transformed points
# ------------------------------------------------------------------------------------------------------------------------------------------


img_height, img_width = 4096, 2304
width = 1300
height = 2000

pts = np.array([[0, 4/5*img_height], [img_width/2.6, img_height/2], [2/3.5*img_width, img_height/2], [3.5/5*img_width, 4/5*img_height]], dtype=np.float32)
dts = np.array([[0, height], [0, 0], [width, 0], [width, height]], dtype=np.float32)
matrix = cv2.getPerspectiveTransform(pts, dts)

transformed_points = []
for points in points_list:
    tr_points = []
    for point in points:
        tr_points.append(get_projected_point(matrix, point))
    transformed_points.append(tr_points)

total_df['Projected_Points'] = transformed_points

# ------------------------------------------------------------------------------------------------------------------------------------------
# Separate the transormed points to calculate size etc.
# ------------------------------------------------------------------------------------------------------------------------------------------

x_list2 = []
y_list2 = []
for points in range(len(transformed_points)):
    unzipped_object = zip(*transformed_points[points])
    unzipped_list = list(unzipped_object)
    x_list2.append(unzipped_list[0])
    y_list2.append(unzipped_list[1])

x_list2 = [list(elem) for elem in x_list2]
y_list2 = [list(elem) for elem in y_list2]

total_df['X_points2'] = x_list2
total_df['Y_points2'] = y_list2

# If topview is no then select the normal points that are not transformed
if top_view == 'No':
    total_df['X_points2'] = x_list
    total_df['Y_points2'] = y_list


# ------------------------------------------------------------------------------------------------------------------------------------------
# Calculate all of the explanatory variables
# ------------------------------------------------------------------------------------------------------------------------------------------

# total_df = total_df.loc[total_df['Severity'].notnull()]

total_df['Area'] = total_df.apply(lambda row: PolyArea(row), axis=1)
total_df['Height'] = total_df.apply(lambda row: get_height(row), axis=1)
total_df['Width'] = total_df.apply(lambda row: get_width(row), axis=1)
total_df['Height_width'] = total_df.apply(lambda row: get_height_width(row), axis=1)
total_df['X_mean'] = total_df.apply(lambda row: get_mean(row, 'X_points2'), axis=1)
total_df['Y_mean'] = total_df.apply(lambda row: get_mean(row, 'Y_points2'), axis=1)
total_df['Xmin'] = total_df.apply(lambda row: get_min_x(row), axis=1)
total_df['Ymin'] = total_df.apply(lambda row: get_min_y(row), axis=1)
total_df['Xmax'] = total_df.apply(lambda row: get_max_x(row), axis=1)
total_df['Ymax'] = total_df.apply(lambda row: get_max_y(row), axis=1)
total_df['X_len'] = total_df.apply(lambda row: number_x_points(row), axis=1)
total_df['Y_len'] = total_df.apply(lambda row: number_y_points(row), axis=1)

total_df['Height2'] = None
total_df['Width2'] = None

for idx in range(0, len(total_df)):
    
    total_df['Height2'].iloc[idx] = total_df.iloc[idx]['Height_width'][0]
    total_df['Width2'].iloc[idx] = total_df.iloc[idx]['Height_width'][1]
    
total_df['Height2'] = total_df['Height2'].astype(int)
total_df['Width2'] = total_df['Width2'].astype(int)

# ------------------------------------------------------------------------------------------------------------------------------------------
# Horus preprocessing
# ------------------------------------------------------------------------------------------------------------------------------------------
if camera == 'Horus':

    total_df.loc[total_df['Defect'] == 'Crack longitudinal', 'Defect'] = '002. Crack longitudinal'
    total_df.loc[total_df['Defect'] == 'Crack transversal', 'Defect'] = '003. Crack transversal'
    total_df.loc[total_df['Defect'] == 'Raveling', 'Defect'] = '001. Raveling'


# ------------------------------------------------------------------------------------------------------------------------------------------
# Save the excel file to output_path
# ------------------------------------------------------------------------------------------------------------------------------------------

total_df = total_df.loc[total_df['Defect'].isin(DEFECT_list)]

total_df.to_csv(OUTPUT_PATH, index=False)

print("Done")