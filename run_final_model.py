import json
import os
import pandas as pd
import numpy as np
from shapely.geometry import Polygon
from centerline.geometry import Centerline
import cv2
from PIL import Image

# from preprocess_functions import *
from tqdm import tqdm

import scipy.misc as mc
import matplotlib.pyplot as plt

from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import *
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint
from tensorflow.keras.layers import Input,Conv2DTranspose, MaxPooling2D,BatchNormalization,concatenate,Activation, Conv2D

from Spatial_Attention import *

from SA_UNet import *



# ------------------------------------------------------------------------------------------------------------------------------------------
# Set your path variables
# ------------------------------------------------------------------------------------------------------------------------------------------


INPUT_PATH = r'C:\Git-Clones\blood-vessel-detection-for-crack-severity\CSV\Nordics\Cyclomedia_CROW_-7.csv'

OUTPUT_PATH = r'C:\Git-Clones\blood-vessel-detection-for-crack-severity\CSV\second_try.xlsx'


# ------------------------------------------------------------------------------------------------------------------------------------------
# Load the blood vessel model
# ------------------------------------------------------------------------------------------------------------------------------------------


desired_size = 512

model=SA_UNet(input_size=(desired_size,desired_size,3),start_neurons=16,lr=1e-3,keep_prob=0.87,block_size=7)
weight="Model/CHASE/SA_UNet.h5"

model.load_weights(weight)



# ------------------------------------------------------------------------------------------------------------------------------------------
# Create flat images
# ------------------------------------------------------------------------------------------------------------------------------------------
csv_df = pd.read_csv(INPUT_PATH, converters={'X_points2': eval, 'Y_points2': eval, 'X_points': eval, 'Y_points': eval})
csv_df = csv_df.loc[csv_df['Severity'].notnull()]


width = 1300
height = 2000

img_height = 2304
img_width = 4096

# If image has horizontal view (0 degrees)
# pts = np.array([[750, 2300], [1940 ,1250], [2220, 1250], [3500, 2200]], dtype=np.float32)

# If image has hood view (-8 degrees)
pts = np.array([[0, 4/5*img_height], [img_width/2.6, img_height/2], [2/3.5*img_width, img_height/2], [3.5/5*img_width, 4/5*img_height]], dtype=np.float32)


dts = np.array([[0, height], [0, 0], [width, 0], [width, height]], dtype=np.float32)
matrix = cv2.getPerspectiveTransform(pts, dts)

# if not os.path.exists(OUTPUT_PATH):
#         os.makedirs(OUTPUT_PATH)

# for img_path in tqdm(IMAGES_PATH):
#     img = cv2.imread(img_path)
#     output = cv2.warpPerspective(img, matrix, (width, height))

for rownumber, row in tqdm(csv_df.iterrows()):
    
    defect1 = csv_df.loc[rownumber]['Image']
    
    # Get the shape, defect and severity
    defect = csv_df.loc[rownumber]['Defect']
    severity = str(int(csv_df.loc[rownumber]["Severity"]))
    

    # Go through image and do the warping
    img_path = csv_df.loc[rownumber]["Image_path"]
    img = cv2.imread(img_path)
    output = cv2.warpPerspective(img, matrix, (width, height))  

    # Get the x and y coordinates of the WARPED image
    x = csv_df.loc[rownumber]["X_points"]
    y = csv_df.loc[rownumber]["Y_points"]

    # Crop the defect out of the image
    crop = []
    for idx in range(len(x)):
        crop.append([x[idx], y[idx]])
    crop = np.array([crop])


    # Images that do not have values should be skipped
    nonetype = type(None)
    if type(img) == nonetype:
        continue

    # Cut the defect out of the FLAT image

    height = img.shape[0]
    width = img.shape[1]
    mask = np.zeros((height, width), dtype=np.uint8)
    points = crop
    cv2.fillPoly(mask, points, (255))
    res = cv2.bitwise_and(img,img,mask = mask)
    rect = cv2.boundingRect(points) # returns (x,y,w,h) of the rect
    shape_img = res[rect[1] : rect[1] + rect[3], rect[0] : rect[0] + rect[2]] # Create shape image

    
    shape_img = cv2.cvtColor(shape_img, cv2.COLOR_BGR2RGB)
    shape_img2 = Image.fromarray(shape_img)
    
    # Get the defect to the center
    old_size = shape_img2.size
    new_size = (512, 512)

    new_im = Image.new("RGB", new_size)   ## luckily, this is already black!
    new_im.paste(shape_img2, ((int((new_size[0]-old_size[0])/2), (int((new_size[1]-old_size[1])/2)))))

    new_im2 = np.asarray(new_im)
    new_im2 = new_im2.astype('float32') / 255.

    # Create the input for the model
    new_list = []
    new_list.append(new_im2)
    img2pred = np.array(new_list)

    # Run the prediction
    prediction = model.predict(img2pred, verbose=1)

    
    # Get the ratio
    start_white = 210 # Set the number when you start counting white pixels
    total1 = prediction[0] * 255
    total1[total1 < start_white] = 0
    count_white = np.sum(total1 > start_white)
    count_total = np.sum(np.mean(img2pred[0], axis=2) > 0)
    
    if count_white != 0:
        ratio = (count_white/count_total)
    else:
        ratio = 0

    print("The ratio is: ", ratio)
    

    csv_df.at[rownumber, 'Ratio'] = ratio
    
csv_df.to_excel(OUTPUT_PATH, index=False)

print("Done")