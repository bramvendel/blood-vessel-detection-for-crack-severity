import os
import gdal

from tqdm import tqdm


# -----------------------------------------------------------------------------------------------------------------------------------------------------------------
# Set the Input/Output Paths
#------------------------------------------------------------------------------------------------------------------------------------------------------------------

# The path to a folder with all of the individual shapefiles for the defects
input_path = r"C:\Users\vendelb5569\OneDrive - ARCADIS\Shapefiles"

# The output path to where the clipped tiff files should be stored
tiffpath = r"C:\Users\vendelb5569\OneDrive - ARCADIS\Tifffiles"

# The path to the large merged tiff file
tiffl = r'N:\AAD\Models\Road\Severity_Model_Top_View_Dataset\N665\merged_bigger.tif'

# The path to the original shape file
ROADS_SHPFL = r"C:\Users\vendelb5569\OneDrive - ARCADIS\Image Recognition\Arcadis_Cyclomedia_Wegdekanalyse_Zeeland_N665_Individuele_Schadebeelden_CROW_146a_v2020.shp"


# -----------------------------------------------------------------------------------------------------------------------------------------------------------------
# Get top view of each defect
#------------------------------------------------------------------------------------------------------------------------------------------------------------------

# Load in the original shapefile
ROADS = gpd.read_file(ROADS_SHPFL)

# Specify the defects as a list
crack_list = ['Langsscheur', 'Dwarsscheur', 'Craquele']



only_cracks_df = ROADS.loc[ROADS['TypeDefect'].isin(crack_list)]

unique_id_list = only_cracks_df['UniekID'].tolist()
severity_list = only_cracks_df['Ernst'].tolist()
defect_list = only_cracks_df['TypeDefect'].tolist()


# Loop over every specific defect in the folder
for shapefile in tqdm(os.listdir(input_path)):

    # Only load the shapefiles
    if shapefile[-3:] == 'shp':

        # Get the unique id for filtering
        unique_id = int(shapefile[8:-4])
        
        
        
        if unique_id in unique_id_list:
            
            place_in_list = unique_id_list.index(unique_id)
            
            severity = severity_list[place_in_list]
            defect = defect_list[place_in_list]

            shpfl = os.path.join(input_path, shapefile)
            
            if not os.path.exists(os.path.join(tiffpath, defect, severity)):
                os.makedirs(os.path.join(tiffpath, defect, severity))
                            
            outfl = os.path.join(tiffpath, defect, severity, shapefile[:-3]) + 'tif'
        
            result = gdal.Warp(outfl, tiffl, cutlineDSName=shpfl, cropToCutline=True)