import json
import os
import pandas as pd
import numpy as np
import xgboost
from shapely.geometry import Polygon
from centerline.geometry import Centerline
import cv2
from PIL import Image

# from preprocess_functions import *
from tqdm import tqdm


# ------------------------------------------------------------------------------------------------------------------------------------------
# Set your path variables
# ------------------------------------------------------------------------------------------------------------------------------------------

base_path = r'C:\Users\vendelb5569\OneDrive - ARCADIS\Tifffiles\Langsscheur\L'
save_path = r'C:\Users\vendelb5569\OneDrive - ARCADIS\Tifffiles\Langsscheur Center\L'

# Set the pixel size of your total square image (HxW)
ideal_size = 512

# ------------------------------------------------------------------------------------------------------------------------------------------
# Get center images
# ------------------------------------------------------------------------------------------------------------------------------------------

if not os.path.exists(save_path):
    os.makedirs(save_path)

for image in tqdm(os.listdir(base_path)):
    old_im = Image.open(base_path + '\\' + image)
    old_size = old_im.size

    new_size = (ideal_size, ideal_size)
    new_im = Image.new("RGB", new_size)   ## luckily, this is already black!
    new_im.paste(old_im, ((int((new_size[0]-old_size[0])/2),
                          (int((new_size[1]-old_size[1])/2)))))

    new_im.save(save_path + '\\'+ image)