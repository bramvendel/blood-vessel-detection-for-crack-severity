import os

import numpy as np
import cv2
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint

np.random.seed(42)

import scipy.misc as mc
import matplotlib.pyplot as plt

from  SA_UNet import *

desired_size = 1008

model=SA_UNet(input_size=(desired_size,desired_size,3),start_neurons=16,lr=1e-3,keep_prob=0.87,block_size=7)
weight="Model/CHASE/SA_UNet.h5"

restore=True

if restore and os.path.isfile(weight):
    model.load_weights(weight)

# model_checkpoint = ModelCheckpoint(weight, monitor='val_accuracy', verbose=1, save_best_only=False)

print(model.summary())