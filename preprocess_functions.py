import json
import os
import pandas as pd
import numpy as np
import xgboost
from shapely.geometry import Polygon
from centerline.geometry import Centerline
import cv2 


def get_projected_point(matrix, point):
    px = (matrix[0][0]*point[0] + matrix[0][1]*point[1] + matrix[0][2]) / ((matrix[2][0]*point[0] + matrix[2][1]*point[1] + matrix[2][2]))
    py = (matrix[1][0]*point[0] + matrix[1][1]*point[1] + matrix[1][2]) / ((matrix[2][0]*point[0] + matrix[2][1]*point[1] + matrix[2][2]))
    return (int(px), int(py))

def PolyArea(row):
    x = row['X_points2']
    y = row['Y_points2']
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))

def get_height(row):
    x = row['X_points2']
    height = np.max(x)-np.min(x)
    return height
    
def get_width(row):
    y = row['Y_points2']
    width = np.max(y)-np.min(y)
    return width


def get_height_width(row):
    x = row['X_points2']
    height = np.max(x)-np.min(x)
    y = row['Y_points2']
    width = np.max(y)-np.min(y)
    if height < width:
        return width, height
    return height, width

def get_min_x(row):
    x = row['X_points2']
    point = np.min(x)
    if point < 0:
        point=0
    return point

def get_min_y(row):
    y = row['Y_points2']
    point = np.min(y)
    if point < 0:
        point=0
    return point

def get_max_x(row):
    x = row['X_points2']
    point = np.max(x)
    if point < 0:
        point=0
    return point

def get_max_y(row):
    y = row['Y_points2']
    point = np.max(y)
    if point < 0:
        point=0
    return point

def number_x_points(row):
    x = row['X_points2']
    return len(x)

def number_y_points(row):
    x = row['Y_points2']
    return len(x)

def get_mean(row, col):
    lijst  = row[col]
    mean = np.mean(lijst)
    return mean

def get_shape(row):
    x = row['X_points2']
    y = row['Y_points2']
    coordinates = []
    for idx in range(0, len(x)):
        coordinates.append([x[idx], y[idx]])
    return coordinates

def get_centerline_length(row):
    poly = row['Coordinates']
    polygon = Polygon(poly)
    centerline = Centerline(polygon, interpolation_distance=0.5)
    
    return centerline.length